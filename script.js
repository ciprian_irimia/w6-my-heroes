const listEroi = document.querySelector(".list-group");
//Get buttons
const getPost = document.querySelector(".get-post");
const addPost = document.querySelector(".add-post-form");
const nume = document.getElementById("nume");
const prenume = document.getElementById("prenume");
const numeErou = document.getElementById("numeErou");
const idSuperputere = document.getElementById("idSuperputere");
const numeSuperputere = document.getElementById("numeSuperputere");
const editPost = document.querySelector(".edit-post");
const deletePost = document.querySelector(".delete-post");

class listaEroi {
  constructor(
    id,
    nume,
    prenume,
    numele_de_erou,
    superputeriId,
    nume_superputere
  ) {
    this.id = id;
    this.nume = nume;
    this.prenume = prenume;
    this.numele_de_erou = numele_de_erou;
    this.superputeriId = superputeriId;
    this.nume_superputere = nume_superputere;
  }
  arataDate() {
    return [
      this.id,
      this.nume,
      this.prenume,
      this.numele_de_erou,
      this.superputeriId,
      this.nume_superputere,
    ];
  }
}

const eroiArray = [];
const url = "http://localhost:3000/eroi/?_expand=superputeri";
let output = "";

getPost.addEventListener("click", function () {
  fetch(url, { method: "GET" })
    .then(function (raspuns) {
      return raspuns.json();
    })
    .then(function (raspunsTipJson) {
      console.log("Rapuns de tip Json:", raspunsTipJson);
      output = "";
      raspunsTipJson.forEach(function (erou, index) {
        console.log(`Eroul de la indexul ${index} este ${erou.numele_de_erou}`);
        eroiArray.push(
          new listaEroi(
            erou.id,
            erou.nume,
            erou.prenume,
            erou.numele_de_erou,
            erou.superputeriId,
            erou.superputeri.nume_superputere
          )
        );

        output += `<li class="list-group-item"><strong>ID erou:</strong> ${erou.id}</li><li class="list-group-item"><strong>Nume:</strong> ${erou.nume}</li><li class="list-group-item"><strong>Prenume:</strong> ${erou.prenume}</li><li class="list-group-item"><strong>Nume de erou:</strong> ${erou.numele_de_erou}</li><li class="list-group-item"><strong>ID superputere:</strong> ${erou.superputeriId}</li><li class="list-group-item"><strong>Nume superputere:</strong> ${erou.superputeri.nume_superputere}</li><div>
          <button class="btn btn-primary edit-post" id="erou-id-${erou.id} "data-bs-toggle="modal" data-bs-target="#editHeroModal">EDIT ${erou.numele_de_erou}</button>
          <button class="btn btn-primary delete-post">DELETE</button>
        </div><br>
        `;

        listEroi.innerHTML = output;
      });
      const editAllButtons = document.querySelectorAll(".edit-post");
      console.log(editAllButtons);
      editAllButtons.forEach(function (buton) {
        buton.addEventListener("click", triggerEditFlow);
      });
      console.log(eroiArray);
    });
});

/*identificam eroul cu id-ul "X"
implementam modalul - strict bootstrap
implementam un form in modal
formul il initializam cu eroul selectat (pasul 1)
pe submit de form punem eventlistener care: 
- construieste noul payload(obiectul erou actualizat);
- apeleaza fetch-ul care face PUT*/

//UPDATE

function triggerEditFlow(event) {
  //get hero id
  const id = Number(event.target.id.split("-").at(-1));
  //Get selected hero
  const selectedHero = eroiArray.find((hero) => hero.id === id);
  const modalBody = document.getElementById("generateForm");
  modalBody.innerHTML = generateForm(selectedHero);
  const saveButton = document.getElementById("saveEditedHero");
  saveButton.addEventListener("click", function () {
    submitHeroChanges(id);
  });

  // const selectedHero = eroiArray.find((hero) => {
  //   //aici mai facem diverse chestii inainte sa returnam ceva
  //   return hero.id === id;
  // });
  console.log(selectedHero);
}

function submitHeroChanges(id) {
  const form = document.querySelector("#generateForm form");
  console.dir(form);
  const nume = form[0].value;
  const prenume = form[1].value;
  const numeErou = form[2].value;
  const patchURL = `http://localhost:3000/eroi/${id}`;
  console.log({ nume, prenume, numeErou, id });
  fetch(patchURL, {
    method: "PATCH",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      nume,
      prenume,
      numele_de_erou: numeErou,
    }),
  });
}
function generateForm(hero) {
  return `
  <form class="add-post-form" >
        <div class="mb-3">
          <label class="form-label"
            ><strong>EDITING...</strong></label
          >
        </div>
        <div class="mb-3">
          <input
            type="text"
            class="form-control"
            id="nume"
            placeholder="Introduceti nume"
            value="${hero.nume}"
            required
          />
        </div>
        <div class="mb-3">
          <input
            type="text"
            class="form-control"
            id="prenume"
            placeholder="Introduceti prenume"
            value="${hero.prenume}"
            required
          />
        </div>
        <div class="mb-3">
          <input
            type="text"
            class="form-control"
            id="numeErou"
            placeholder="Introduceti nume de erou"
            value="${hero.numele_de_erou}"
            required
          />
        </div>
      </form>
  `;
}

// EDIT;
// editPost.addEventListener("click", function (e) {
//   console.log(e.target.id);
// });

//POST
addPost.addEventListener("submit", function (e) {
  e.preventDefault();
  console.log("Form submited");
  console.log(nume.value);
  fetch(url, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      nume: nume.value,
      prenume: prenume.value,
      numele_de_erou: numeErou.value,
      // nume_superputere: numeSuperputere.value,
      superputeriId: Number(idSuperputere.value),
    }),
  })
    .then(function (raspuns) {
      console.log(raspuns.json());
      return raspuns.json();
    })
    .catch(function (err) {
      console.log("Catch me if you ...,", err);
    });
});
